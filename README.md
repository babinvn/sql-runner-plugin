# SQL Runner

This is SQL Runner plugin for Atlassian Confluence / JIRA

This plugin allows you to run SQL commands in Confluence / JIRA database.

!!! DO NOT USE IN PRODUCTION !!!

## Usage:

Confluence:

```
curl -X POST -d " \
show columns from bandana \
" -H 'Content-Type: text/plain' -H 'X-Atlassian-Token: no-check' http://localhost/wiki/rest/runsql/1.0/
```

JIRA:

```
curl -X POST -d " \
show columns from pluginversion \
" -H 'Content-Type: text/plain' -H 'X-Atlassian-Token: no-check' http://localhost/servicedesk/rest/runsql/1.0/
```

You may run DDL statements as well.
