package me.bvn.sql;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import java.io.IOException;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

@Path("/")
public class SqlRunnerResource {
    private final SqlRunnerService sqlRunner;
    private final ObjectMapper mapper = new ObjectMapper();

    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.TEXT_PLAIN})
    public Response getMessage(String sql) throws SQLException, IOException {
       return Response.ok(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sqlRunner.runSql(mapper, sql))).build();
    }
    
    public SqlRunnerResource(SqlRunnerService sqlRunner) {
        this.sqlRunner = sqlRunner;
    }
}