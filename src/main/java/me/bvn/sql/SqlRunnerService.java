package me.bvn.sql;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public interface SqlRunnerService {
    JsonNode runSql(ObjectMapper mapper, String sql, Object... args);
}