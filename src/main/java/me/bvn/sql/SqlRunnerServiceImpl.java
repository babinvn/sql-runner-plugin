package me.bvn.sql;

import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.spring.container.ContainerManager;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.util.StdDateFormat;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.ofbiz.core.entity.GenericDataSourceException;
import org.ofbiz.core.entity.jdbc.SQLProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

public class SqlRunnerServiceImpl implements SqlRunnerService, InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlRunnerServiceImpl.class);

    private boolean isConfluence = false;
    private boolean isJira = false;
    private final StdDateFormat dateFormat = new StdDateFormat();

    @Override
    public JsonNode runSql(ObjectMapper mapper, String sql, Object... args) {
        if (isConfluence) {
            return runSqlConfluence(mapper, sql.trim(), args);
        } else if (isJira) {
            return runSqlJira(mapper, sql.trim(), args);
        } else {
            throw new RuntimeException("Application not supported");
        }
        
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            ContainerManager.getComponent("hibernateSessionManager");
            isConfluence = true;
            return;
        } catch (Throwable ex) {
            // Not Confluence
        }
        isJira = true;
    }

    private JsonNode runSqlConfluence(ObjectMapper mapper, String sql, Object... args) {
        try {
            Object hibernateSessionManager = ContainerManager.getComponent("hibernateSessionManager");
            TransactionTemplate template = (TransactionTemplate)ContainerManager.getComponent("transactionTemplate");
            return template.execute(() -> {
                try {
                    // Connection conn = hibernateSessionManager.getSession().connection();
                    Method getSession = hibernateSessionManager.getClass().getMethod("getSession");
                    Object session = getSession.invoke(hibernateSessionManager);
                    Method connection = session.getClass().getDeclaredMethod("connection");
                    Connection conn = (Connection)connection.invoke(session);
                    return runSql(conn, mapper, sql, args);
                } catch (Throwable ex) {
                    LOGGER.error(String.format("Error in SQL: %s", sql), ex);
                    return exception(mapper, ex);
                }
            });
        } catch (Throwable ex) {
            LOGGER.error(String.format("Error in SQL: %s", sql), ex);
            return exception(mapper, ex);
        }
    }
    private JsonNode runSqlJira(ObjectMapper mapper, String sql, Object... args) {
        SQLProcessor processor = new SQLProcessor("defaultDS");
        try {
            Connection conn = processor.getConnection();
            return runSql(conn, mapper, sql, args);
        } catch (Throwable ex) {
            LOGGER.error(String.format("Error in SQL: %s", sql), ex);
            return exception(mapper, ex);
        } finally {
            // processor.close();
            closeProcessor(processor);
        }
    }
    
    private ObjectNode affected(ObjectMapper mapper, int count) {
        ObjectNode obj = mapper.createObjectNode();
        obj.put("affected", count);
        return obj;
    }
    private ObjectNode exception(ObjectMapper mapper, Throwable ex) {
        ObjectNode obj = mapper.createObjectNode();
        obj.put("error", ex.getMessage());
        return obj;
    }
    private void closeProcessor(SQLProcessor processor) {
        try {
            processor.close();
        } catch (GenericDataSourceException ex) {
            LOGGER.error("Error closing SQLProcessor", ex);
        }
    }
    private JsonNode runSql(Connection conn, ObjectMapper mapper, String sql, Object... args) {
        try {
            if (sql.toLowerCase().startsWith("select") || sql.toLowerCase().startsWith("show") || sql.toLowerCase().startsWith("desc")) {
                ArrayNode arr = mapper.createArrayNode();
                try (
                    PreparedStatement ps = conn.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery();
                ) {
                    while (rs.next()) {
                        ObjectNode obj = mapper.createObjectNode();
                        for (int column = 1; column <= rs.getMetaData().getColumnCount(); column++) {
                            putValue(obj, rs, column);
                        }
                        arr.add(obj);
                    }
                }
                return arr;
            } else {
                try (CallableStatement cs = conn.prepareCall(sql)) {
                    int count = cs.executeUpdate();
                    return affected(mapper, count);
                }
            }
        } catch (SQLException ex) {
            LOGGER.error(String.format("Error in SQL: %s", sql), ex);
            return exception(mapper, ex);
        }
    }
    private void putValue(ObjectNode obj, ResultSet rs, int column) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        String columnName = meta.getColumnName(column);

        switch (meta.getColumnType(column)) {
            case Types.BIGINT:
            case Types.INTEGER:
            case Types.SMALLINT:
            case Types.TINYINT:
                obj.put(columnName, rs.getLong(column));
                break;
            case Types.DECIMAL:
            case Types.DOUBLE:
            case Types.FLOAT:
            case Types.NUMERIC:
            case Types.REAL:
                obj.put(columnName, rs.getDouble(column));
                break;
            case Types.TIMESTAMP:
                Timestamp timestamp = rs.getTimestamp(column);
                obj.put(columnName, dateFormat.format(new Date(timestamp.getTime())));
                break;
            case Types.DATE:
                Date date = rs.getDate(column);
                obj.put(columnName, dateFormat.format(date));
                break;
            case Types.TIME:
                Time time = rs.getTime(column);
                obj.put(columnName, time.getTime());
                break;
            default:
                obj.put(columnName, rs.getString(column));
                
        }
    }
}
